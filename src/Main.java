package net.explodingbush.juicebox;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import javax.security.auth.login.LoginException;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Category;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.MessageHistory;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.exceptions.RateLimitedException;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.dv8tion.jda.core.managers.GuildController;
import net.dv8tion.jda.core.requests.RestAction;

public class Main extends ListenerAdapter {
	
	public static void main(String[] args) 
            throws LoginException, RateLimitedException, InterruptedException
    {
		@SuppressWarnings("unused")
		JDA jda = new JDABuilder(AccountType.BOT).setToken("NDY3NzIxNDE3MDYxMDQwMTMw.DjHQvg.kkjfZR8c6vQCZgeQ36_PbvQeQDI").addEventListener(new Main()).buildBlocking();
    }
	@Override
    public void onReady(ReadyEvent e)
    {
        e.getJDA().getPresence().setPresence(Game.watching("some Netflix"), true);    
    	System.out.println("Juice Box is ready to go!");
    }
	@Override
    public void onMessageReceived(MessageReceivedEvent e) {
    	MessageChannel channel = e.getChannel();
    	Message message = e.getMessage();
    	Guild guild = message.getGuild();
    	GuildController controller = message.getGuild().getController();
    	Member member = e.getMember();
    	User author = e.getAuthor();
    	String content = message.getContentRaw();
    	String[] args = message.getContentRaw().split("\\s+");
    	List<TextChannel> audit = guild.getTextChannelsByName("audit-logs", true);
    	Consumer<Message> deleteLater = msg -> msg.delete().queueAfter(5, TimeUnit.SECONDS);
    	Member target = message.getMentionedMembers().get(0);
    	String noperm = "Oops, you don't have permission to use this command!";
    	if(!author.isBot()) {
    		if(content.startsWith("!setgame")) {
    			message.delete().queue();
    			if(!(args.length > 1)) {
    				channel.sendMessage("You need to provide a game for the bot to play!").queue(deleteLater);
    			}
    				e.getJDA().getPresence().setGame(Game.playing(String.join(" ", Arrays.asList(args).subList(1, args.length))));
    				channel.sendMessage("Game successfully set to " + "**" + String.join(" ", Arrays.asList(args).subList(1, args.length)) + "**").queue(deleteLater);
    		}
    		if(content.startsWith("!ban")) {
    			message.delete().queue();
    			if(!member.hasPermission(Permission.BAN_MEMBERS)) {
    				channel.sendMessage(noperm).queue(deleteLater);
    				return;
    			} 
    			if(!(message.getMentionedMembers().toArray().length == 1)) {
    				channel.sendMessage("You either forgot to mention a member to ban!").queue(deleteLater);
    			} else {
    				if(!(args.length > 2)) {
    					channel.sendMessage("You forgot to provide a reason to ban this member!").queue(deleteLater);
    				} else {
    					
    					String banreason = String.join(" ", Arrays.asList(args).subList(2, args.length));
    					MessageEmbed banembed = new EmbedBuilder()
    							.setAuthor("Member Banned", null, author.getAvatarUrl())
    							.setColor(0xFF470F).addField("User", target.getAsMention(), true)
    							.addField("Moderator", member.getAsMention().toString(), true)
    							.addField("Reason", banreason, false)
    							.setTimestamp(Instant.now())
    							.build();
    					if(!audit.isEmpty()) {
    						if(member.canInteract(target)) {
    							startPunishment(audit, message, member, "bann", banembed, banreason);
    							controller.ban(target, 0, banreason).queueAfter(500, TimeUnit.MILLISECONDS);
    						} else {
    							channel.sendMessage("Cannot ban this member. I'm missing permissions or your role is lower than the person you're trying to ban!").queue(deleteLater);
    						}
    					}
    				}
    			}
    		}
    		if(content.startsWith("!unban")) { 
    			message.delete().queue();
    			if(!member.hasPermission(Permission.BAN_MEMBERS)) {
    				channel.sendMessage(noperm).queue(deleteLater);
    				return;
    			}
    			if(!(message.getMentionedMembers().toArray().length == 1)) {
    				channel.sendMessage("You either forgot to mention a member to unban!").queue(deleteLater);
    				return;
    		}
					MessageEmbed unbanembed = new EmbedBuilder()
							.setAuthor("Member Unbanned", null, author.getAvatarUrl())
							.setColor(0xFF470F).addField("User", target.getAsMention(), true)
							.addField("Moderator", member.getAsMention().toString(), true)
							.setTimestamp(Instant.now())
							.build();
					if(!audit.isEmpty()) {
						if(member.canInteract(target)) {
							controller.unban(target.getUser()).queue();
							audit.get(0).sendMessage(unbanembed).queue();
							channel.sendMessage("I've healed the wounds of " + target.getAsMention()).queue(deleteLater);
						} else {
							channel.sendMessage("Cannot unban this member. I'm missing permissions or your role is lower than the person you're trying to unban!").queue(deleteLater);
						}
					}
    		}
    		if(content.startsWith("!kick")) {
    			message.delete().queue();
    				if(!member.hasPermission(Permission.KICK_MEMBERS)) {
    					channel.sendMessage(noperm).queue(deleteLater);
    					return;
    				} 
    				if(!(message.getMentionedMembers().toArray().length == 1)) {
    					channel.sendMessage("You either forgot to mention a member to kick!").queue(deleteLater);
    				} else {
    					if(!(args.length > 2)) {
    						channel.sendMessage("You forgot to provide a reason to kick this member!").queue(deleteLater);
    					} else {
    						String kickreason = String.join(" ", Arrays.asList(args).subList(2, args.length));
    						MessageEmbed kickembed = new EmbedBuilder()
    								.setAuthor("Member Kicked", null, author.getAvatarUrl())
    								.setColor(0xFF470F).addField("User", target.getAsMention(), true)
    								.addField("Moderator", member.getAsMention().toString(), true)
    								.addField("Reason", kickreason, false)
    								.setTimestamp(Instant.now())    							
    								.build();
    						if(!audit.isEmpty()) {
    							if(member.canInteract(target)) {
    								startPunishment(audit, message, member, "kick", kickembed, kickreason);
    								controller.kick(target, kickreason).queueAfter(500, TimeUnit.MILLISECONDS);
    							} else {
    								channel.sendMessage("Cannot kick this member. I'm missing permissions or your role is lower than the person you're trying to ban!").queue(deleteLater);
    						}
    					}
    				}
    			}
    		}
    		
    		if(content.startsWith("!mute")) {
    		    message.delete().queue();
    		    if(!member.hasPermission(Permission.VOICE_MUTE_OTHERS)) {
    		        channel.sendMessage(noperm).queue(deleteLater);
    		        return;
    		    } 
    		    if(!(message.getMentionedMembers().size() == 1)) {
    		        channel.sendMessage("You either forgot to mention a member to mute!").queue(deleteLater);
    		        return;
    		    }
    		    if (args.length == 2) {
    		        channel.sendMessage("You forgot to provide a time to mute this member!").queue(deleteLater);
    		        return;
    		    }
    		    if (args.length == 3) {
    		        channel.sendMessage("You forgot to provide a reason to mute this member!").queue(deleteLater);
    		        return;
    		    }
    		    if (!member.canInteract(target)) {
    		        channel.sendMessage("Cannot mute this member. I'm missing permissions or your role is lower than the person you're trying to mute!").queue(deleteLater);
    		        return;
    		    }
    		    String mutereason = String.join(" ", Arrays.asList(args).subList(3, args.length));
    		    long mutedlength = Integer.parseInt(args[2]);
    		    EmbedBuilder mutebuild = new EmbedBuilder()
    		        .setAuthor("Member Muted", null, author.getAvatarUrl())
    		        .setColor(0xFF470F)
    		        .addField("User", target.getAsMention(), true)
    		        .addField("Moderator", member.getAsMention().toString(), true)
    		        .setTimestamp(Instant.now());
    		    Role muted = guild.getRolesByName("Muted", true).get(0);
    		    controller.addSingleRoleToMember(target, muted).queue();
    		    RestAction<Void> action = controller.removeSingleRoleFromMember(target, muted);
    		    	if(!(mutedlength > 1)) {
    		    		mutebuild.addField("Length", "**" +  mutedlength + "** minute", true);
        		        mutebuild.addField("Reason", mutereason, true);
    		    	} else {
    		    		mutebuild.addField("Length", "**" + mutedlength + "** minutes", true);
        		        mutebuild.addField("Reason", mutereason, true);
    		    	}
    		    	if(member.canInteract(muted)) {
    	    		    MessageEmbed unmuteembed = new EmbedBuilder()
    	        		        .setAuthor("Member Unmuted", null, author.getAvatarUrl())
    	        		        .setColor(0x22CB5D)
    	        		        .addField("User", target.getAsMention(), true)
    	        		        .addField("Moderator", guild.getMemberById("467721417061040130").getAsMention(), true)
    	        		        .setTimestamp(Instant.now())
    	    		    		.build();
    		        action.queueAfter(mutedlength, TimeUnit.MINUTES);
    		        target.getUser().openPrivateChannel().queue(dm -> dm.sendMessage("You've been unmuted on **" + guild.getName() + "**!").queueAfter(mutedlength, TimeUnit.MINUTES));
    		        audit.get(0).sendMessage(unmuteembed).queue();
    		    	} else {
    		    		channel.sendMessage("Cannot mute this member. I'm missing permissions or your role is lower than the person you're trying to mute!").queue(deleteLater);
    		    		return;
    		    	}
    		       
    		    MessageEmbed muteembed = mutebuild.build();
    		    startPunishment(audit, message, member, "mut", muteembed, mutereason);
    		}
    		if(content.startsWith("!unmute")) {
    			message.delete().queue();
    			if(!member.hasPermission(Permission.VOICE_MUTE_OTHERS)) {
    				channel.sendMessage(noperm).queue(deleteLater);
    				return;
    			}
    			if(args.length == 0) {
    				channel.sendMessage("You need to mention a member to unmute!").queue(deleteLater);
    			}
    	    	Role muted = guild.getRolesByName("Muted", true).get(0);
    	    	if(!target.getRoles().contains(muted)) {
    	    		channel.sendMessage(target.getAsMention() + " isn't muted!").queue(deleteLater);
    	    		return;
    	    	}
    			controller.removeSingleRoleFromMember(target, muted).queue();
    		    channel.sendMessage("I've untapped " + target.getAsMention() + "'s mouth.").queue(deleteLater);
    		    MessageEmbed unmuteembed = new EmbedBuilder()
        		        .setAuthor("Member Unmuted", null, author.getAvatarUrl())
        		        .setColor(0x22CB5D)
        		        .addField("User", target.getAsMention(), true)
        		        .addField("Moderator", member.getAsMention().toString(), true)
        		        .setTimestamp(Instant.now())
    		    		.build();
    		    audit.get(0).sendMessage(unmuteembed).queue();
    			target.getUser().openPrivateChannel().queue(dm -> dm.sendMessage("You've been unmuted on **" + guild.getName() + "**!").queue());
    		}
    		if(content.startsWith("!warn")) {
    			message.delete().queue();
    			if(!member.hasPermission(Permission.KICK_MEMBERS)) {
					channel.sendMessage(noperm).queue(deleteLater);
				} 
				if(!(message.getMentionedMembers().toArray().length == 1)) {
					channel.sendMessage("You either forgot to mention a member to warn").queue(deleteLater);
				} else {
					if(!(args.length > 2)) {
						channel.sendMessage("You forgot to provide a reason to warn this member!").queue(deleteLater);
					} else {
						String warnreason = String.join(" ", Arrays.asList(args).subList(2, args.length));
						MessageEmbed warnembed = new EmbedBuilder()
								.setAuthor("Member Warned", null, author.getAvatarUrl())
								.setColor(0xFF470F).addField("User", target.getAsMention(), true)
								.addField("Moderator", member.getAsMention().toString(), true)
								.addField("Reason", warnreason, false)
								.setTimestamp(Instant.now())    							
								.build();
						if(!audit.isEmpty()) {
							if(member.canInteract(target)) {
								startPunishment(audit, message, member, "warn", warnembed, warnreason);
							} else {
								channel.sendMessage("Cannot warn this member. I'm missing permissions or your role is lower than the person you're trying to warn!").queue(deleteLater);
							}
						}
					}
				}
    		}
    		if(content.startsWith("!create")) {
    			message.delete().queue();
    			if(args.length < 2 && !guild.getCategoriesByName("Juice Box Channels", false).isEmpty()) {
    				channel.sendMessage("You forgot a channel name!").queue(deleteLater);
    			}
    				if(guild.getCategoriesByName("Juice Box Channels", false).isEmpty()) {
    					controller.createCategory("Juice Box Channels").complete();
    					controller.createTextChannel(String.join(" ", Arrays.asList(args).subList(1, args.length))).setParent(guild.getCategoriesByName("Juice Box Channels", false).get(0)).queue();
    					channel.sendMessage("Text channel **" + String.join(" ", Arrays.asList(args).subList(1, args.length)) + "** created.").queue(deleteLater);
    				} else {
    					Category juice = guild.getCategoriesByName("Juice Box Channels", false).get(0);
    					controller.createTextChannel(String.join(" ", Arrays.asList(args).subList(1, args.length))).setParent(juice).complete();
    					guild.getTextChannelsByName(String.join(" ", Arrays.asList(args).subList(1, args.length)), false).get(0).sendMessage("**This channel was created by ** " + member.getAsMention()).queue();
    					channel.sendMessage("Text channel **" + String.join(" ", Arrays.asList(args).subList(1, args.length)) + "** created.").queue(deleteLater);
    				}
    		}
    		if(content.startsWith("!delete")) {
    			message.delete().queue();
    			if(args.length < 2 && !guild.getCategoriesByName("Juice Box Channels", false).isEmpty()) {
    				channel.sendMessage("You forgot a channel name!").queue(deleteLater);
    				return;
    			}
    			if(guild.getTextChannelsByName(String.join(" ", Arrays.asList(args).subList(1, args.length)), false).isEmpty()) {
    				channel.sendMessage("Text channel **" + String.join(" ", Arrays.asList(args).subList(1, args.length)) + "** doesn't exist!").queue(deleteLater);
    				return;
    			}
    					guild.getTextChannelsByName(String.join(" ", Arrays.asList(args).subList(1, args.length)), false).get(0).delete().complete();
    					channel.sendMessage("Text channel **" + String.join(" ", Arrays.asList(args).subList(1, args.length)) + "** deleted.").queue(deleteLater);
    				}
    		if(content.startsWith("!purge")) {
    			message.delete().queue();
    			if(!member.hasPermission(Permission.MESSAGE_MANAGE)) {
    				channel.sendMessage(noperm).queue(deleteLater);
    				return;
    			}
    			if(args.length == 1) {
    				channel.sendMessage("You forgot to specify an amount of messages to delete.").queue(deleteLater);
    				return;
    			}
    				if(Integer.parseInt(args[1]) == 1) {
    					channel.sendMessage("Cannot delete only a single message.").queue(deleteLater);
    					return;
    				}
    	            MessageHistory history = new MessageHistory(e.getTextChannel());
    	            List<Message> msgs;
    	                        msgs = history.retrievePast(Integer.parseInt(args[1])).complete();
    	                        e.getTextChannel().deleteMessages(msgs).queue();
    	                        channel.sendMessage("**" + args[1] + "** messages deleted!").queue(deleteLater);
    		}
    		if(content.startsWith("!battletower")) {
    			message.delete().queue();
    			if(!(guild.getName().contains("TheAntiBreathingNonThots"))) {
    				channel.sendMessage("This command is specific for **TheAntiBreathingNonThots**!").queue(deleteLater);
    				return;
    			}
    			if(message.getMentionedMembers().size() == 0) {
    				Role battletower = guild.getRolesByName("Playing Battle Tower", true).get(0);
    				if(member.getRoles().contains(battletower)) {
    					controller.removeSingleRoleFromMember(member, battletower).queue();
    					channel.sendMessage("Thanks for playing Battle Tower " + member.getAsMention() + "! Hope you had a fun time!").queue(deleteLater);
    				} else {
    					controller.addSingleRoleToMember(member, battletower).queue();
    					channel.sendMessage("Get ready to have the hardest experience in your life " + member.getAsMention() + "!").queue(deleteLater);
    				}
    			}
    				Role battletower = guild.getRolesByName("Playing Battle Tower", true).get(0);
    				if(target.getRoles().contains(battletower)) {
    					controller.removeSingleRoleFromMember(target, battletower).queue();
    					channel.sendMessage(member.getAsMention() + " has ended the fun for " + target.getAsMention() + "... Hope you had a fun time!").queue(deleteLater);
    				} else {
    					controller.addSingleRoleToMember(target, battletower).queue();
    					channel.sendMessage("You, " + target.getAsMention() + " have summoned by " + member.getAsMention() + " to play Battle Tower! You'll have a great time!").queue(deleteLater);
    			}
    		}
    	}
	}
		public void startPunishment(List<TextChannel> audit, Message message, Member member, String type, MessageEmbed muteembed, String reason) {
			MessageChannel channel = message.getChannel();
			Consumer<Message> deleteLater = msg -> msg.delete().queueAfter(5, TimeUnit.SECONDS);
				audit.get(0).sendMessage(muteembed).queue();
				Member mentionee = message.getMentionedMembers().get(0);
				mentionee.getUser().openPrivateChannel().queue((dm) -> dm.sendMessage("You've been " + type + "ed on **"
						+ mentionee.getGuild().getName()
						+ "** by " 
						+ member.getAsMention() + " for \n**" + reason + "**").queue());
				switch(type) {
				case "bann": channel.sendMessage("I've fired the railguns at " + mentionee.getAsMention() + ".").queue(deleteLater);
				break;
				case "kick": channel.sendMessage("I've smashed " + mentionee.getAsMention() + " with a stick.").queue(deleteLater);
				break;
				case "mut": channel.sendMessage("I've taped " + mentionee.getAsMention() + " mouth closed.").queue(deleteLater);
				break;
				case "warn": channel.sendMessage("I've taped threw my popcorn at " + mentionee.getAsMention() + ".").queue(deleteLater);
				break;
				}
		}
}